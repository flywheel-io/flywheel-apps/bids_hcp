"""Install Freesurfer license.txt file where algorithm expects it."""

import logging
import os
from pathlib import Path

from flywheel_gear_toolkit.utils.zip_tools import unzip_archive

# from flywheel_gear_toolkit.hpc import singularity
from fw_gear_bids_hcp import tmp_singularity

log = logging.getLogger(__name__)


def check_subjects_dir_from_zip_file_path(writable_dir: Path, previous_results: Path):
    """Regardless of prior results, set the proper files in the FS subjects_dir.
    Args:
        writable_dir (Path)
    """
    # TODO Do these files need to be copied into writable_dir?
    paths = list(Path(previous_results).glob("*"))

    if paths:
        log.info("Using provided Freesurfer subject file %s", str(paths[0]))
        unzip_dir = writable_dir / "unzip-fs-subjects-dir"
        unzip_dir.mkdir(parents=True, exist_ok=True)
        unzip_archive(paths[0], unzip_dir)
        for a_subject in unzip_dir.glob("*/*"):
            if Path(os.environ["SUBJECTS_DIR"], a_subject.name).exists():
                log.info("Found %s but using existing", a_subject.name)
            else:
                log.info("Found %s", a_subject.name)
                a_subject.rename(Path(os.environ["SUBJECTS_DIR"], a_subject.name))


def set_FS_templates(orig_subject_dir: Path):
    """Set the template files for FreeSurfer"""
    for template in ("fsaverage", "fsaverage5", "fsaverage6"):
        tmp_singularity.mount_file(orig_subject_dir, Path(os.environ["SUBJECTS_DIR"]), template)


def setup_freesurfer_for_singularity(writable_dir: Path, previous_results: Path = None):
    """Overarching method to set, link, and unzip FreeSurfer-associated files.
    Args:
        writable_dir (Path): may be /var/tmp or a user-specified directory, where Singularity
                will be able to write any number of files.
    """
    new_subj_dir = Path(writable_dir, "freesurfer", "subjects")
    os.environ["SUBJECTS_DIR"] = str(new_subj_dir)
    set_FS_templates(Path(os.environ["SUBJECTS_DIR"]))
    if previous_results:
        check_subjects_dir_from_zip_file_path(Path(writable_dir, os.environ["SUBJECTS_DIR"]), previous_results)
