"""The functional analysis has two major methods. Test each."""

import logging
from unittest.mock import patch

import pytest

from fw_gear_bids_hcp.hcp_func import (
    GenericfMRISurfaceProcessingPipeline,
    GenericfMRIVolumeProcessingPipeline,
)

log = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "test_name, test_fn, num_params, mock_babel",
    [
        (
            "vol_setParams",
            GenericfMRIVolumeProcessingPipeline.set_params,
            21,
            "GenericfMRIVolumeProcessingPipeline",
        ),
        (
            "surf_setParams",
            GenericfMRISurfaceProcessingPipeline.set_params,
            9,
            "GenericfMRISurfaceProcessingPipeline",
        ),
    ],
)
def test_basicSetParams(test_name, test_fn, num_params, mock_babel, mock_gear_args):
    """Are the parameters set, when there are no strange value cases?"""
    with patch("fw_gear_bids_hcp.hcp_func." + mock_babel + ".nibabel.load") as babel:
        babel.return_value.get_header.return_value.get_zooms.return_value = [1, 2, 3]
        params = test_fn(mock_gear_args)
    assert len(params) == num_params


@pytest.mark.parametrize(
    "test_name, test_fn, mocked_exec, other_mocks",
    [
        (
            "vol_exec",
            GenericfMRIVolumeProcessingPipeline.execute,
            "fw_gear_bids_hcp.hcp_func.GenericfMRIVolumeProcessingPipeline.exec_command",
            "fw_gear_bids_hcp.hcp_func.GenericfMRIVolumeProcessingPipeline.build_command_list",
        ),
        (
            "surf_exec",
            GenericfMRISurfaceProcessingPipeline.execute,
            "fw_gear_bids_hcp.hcp_func.GenericfMRISurfaceProcessingPipeline.exec_command",
            "fw_gear_bids_hcp.hcp_func.GenericfMRISurfaceProcessingPipeline.build_command_list",
        ),
    ],
)
@patch("fw_gear_bids_hcp.hcp_func.GenericfMRIVolumeProcessingPipeline.os.makedirs")
def test_execute(mock_mk, test_name, test_fn, mocked_exec, other_mocks, mock_gear_args):
    """Does the execution block build and run the command?
    Error handling is in the modality's main.py"""
    with patch(other_mocks):
        with patch(mocked_exec) as me:
            test_fn(mock_gear_args)
    me.assert_called_once()


@pytest.mark.parametrize(
    "dcmethod, extra_field1, extra_field2, expected_bc_entry",
    [
        ("SiemensFieldMap", "fmapmag", "fmapphase", "NONE"),
        ("TOPUP", "SEPhasePos", "SEPhaseNeg", "SEBASED"),
    ],
)
def test_validate_func_dcmethod_selects_method_no_warns(
    dcmethod, extra_field1, extra_field2, expected_bc_entry, mock_gear_args, caplog
):
    params = {
        "dcmethod": dcmethod,
        extra_field1: "something",
        extra_field2: "something_else",
        "biascorrection": expected_bc_entry,
        "topupconfig": "do not throw an error",
        "unwarpdir": "any direction you like",
    }
    return_params = GenericfMRIVolumeProcessingPipeline.validate_func_dcmethod(mock_gear_args, params)
    assert return_params["biascorrection"] == expected_bc_entry
    assert not any(rec.levelno == logging.ERROR for rec in caplog.records)


@pytest.mark.parametrize(
    "dcmethod, extra_field1, extra_field2, expected_num_errors",
    [
        ("SiemensFieldMap", "fmapmag", "fmapphase", 1),
        ("TOPUP", "SEPhasePos", "SEPhaseNeg", 3),
    ],
)
def test_validate_func_dcmethod_logs_errors(
    dcmethod, extra_field1, extra_field2, expected_num_errors, mock_gear_args, caplog
):
    params = {
        "dcmethod": dcmethod,
        extra_field1: None,
        extra_field2: None,
        "biascorrection": "very biased",
        "fmritcs": "a loooooong scan",
    }
    _ = GenericfMRIVolumeProcessingPipeline.validate_func_dcmethod(mock_gear_args, params)
    assert len([rec.levelno == logging.ERROR for rec in caplog.records]) == expected_num_errors
