import logging
from unittest.mock import patch

from fw_gear_bids_hcp.utils.results import zip_output


@patch("fw_gear_bids_hcp.utils.results.ZipFile")
@patch("fw_gear_bids_hcp.utils.results.os")
def test_zip_output(mock_os, mock_ZF, caplog):
    mock_os.walk.return_value = [("make_believe/root", "discarded", ["happy", "dance"])]
    mock_os.stat.return_value.st_size = 1
    zip_output("UV-rays", "number1", "/put/it/here", "/bids/bids/bids", None)

    assert mock_os.remove.call_count == 1
    assert mock_os.chdir.call_count == 1
    assert mock_ZF.call_count == 1
    # files should be "happy" and "dance"
    assert len([rec for rec in caplog.records if rec.levelno == logging.DEBUG]) == 2
