from unittest.mock import patch

from fw_gear_bids_hcp.utils.gear_arg_utils import sort_gear_args


def test_GearArgs_instantiated_properly(mock_bare_GearArgs, mock_gtk_context):
    test_GearArgs = mock_bare_GearArgs()
    test_GearArgs(mock_gtk_context)
    assert test_GearArgs.add_templates.called
    assert test_GearArgs.add_processing_steps.called
    assert test_GearArgs.add_context_info.called
    assert test_GearArgs.run_updates.called


@patch("os.path.isfile", return_value=True)
def test_add_templates_works(mock_isfile, mock_bare_GearArgs, mock_gtk_context):
    setup_GearArgs = mock_bare_GearArgs(excl="add_templates")
    test_GearArgs = setup_GearArgs(mock_gtk_context)
    assert test_GearArgs.templates["template2mmmask"] == "pipe/temp/MNI152_T1_2mm_brain_mask_dil.nii.gz"
    assert test_GearArgs.templates["freesurfer_labels"] == "pipe/config/FreeSurferAllLut.txt"
    assert test_GearArgs.templates["t1templatebrain0.8mm"] == "pipe/temp/MNI152_T1_0.8mm_brain.nii.gz"


def test_add_processing_steps_works1(mock_bare_GearArgs, mock_gtk_context):
    setup_GearArgs = mock_bare_GearArgs(excl="add_processing_steps")
    test_GearArgs = setup_GearArgs(mock_gtk_context)
    assert test_GearArgs.processing["common_command"] == [
        "fossil/bin/fsl_sub",
        "-l",
        "test/data/bids/logs",
    ]


def test_add_processing_steps_works2(mock_bare_GearArgs, mock_gtk_context, mock_gear_args):
    setup_GearArgs = mock_bare_GearArgs(excl="add_context_info")
    tmp_dict = {}
    for msr in ["common", "diffusion", "functional", "structural"]:
        field = getattr(mock_gear_args, msr)
        tmp_dict[msr] = field
    return_dict = sort_gear_args(tmp_dict)
    with patch("fw_gear_bids_hcp.utils.gear_arg_utils.sort_gear_args", return_value=return_dict):
        test_GearArgs = setup_GearArgs(mock_gtk_context)
    assert len(test_GearArgs.functional) == 2
