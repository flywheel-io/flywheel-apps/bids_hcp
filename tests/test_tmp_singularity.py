import os
from pathlib import Path
from unittest.mock import patch

import pytest

from fw_gear_bids_hcp import tmp_singularity


def test_check_for_singularity(caplog):
    """Unit test for check_for_singularity"""
    # unless "SINGULARITY_NAME" exists in the environment, it should return false:
    assert not tmp_singularity.check_for_singularity()
    assert "Singularity not detected." in caplog.text

    # if SINGULARITY_NAME is an env variable, it should return True:
    with patch.dict(os.environ, {"SINGULARITY_NAME": "Hola"}):
        assert tmp_singularity.check_for_singularity()
    assert "Singularity detected." in caplog.text


def test_check_writable_dir(tmpdir):
    """Unit test for check_writable_dir"""
    # when passing a writable dir, it should return True:
    assert tmp_singularity.check_writable_dir(tmpdir)
    # when passing a non-writable dir, it should return False:
    assert not tmp_singularity.check_writable_dir("/sys")


def test_log_singularity_details(caplog):
    """Unit test for log_singularity_details"""
    with patch.dict(os.environ, {"SINGULARITY_NAME": "Hola"}):
        tmp_singularity.log_singularity_details()
    assert "SINGULARITY_NAME is Hola" in caplog.text


def test_mount_file(tmpdir, caplog):
    """Unit test for mount_file"""
    old_path = Path(tmpdir, "foo")
    new_path = Path(tmpdir, "bar")
    filename = "fam.txt"
    old_path.mkdir()
    (old_path / filename).touch()
    # if "filename" does not exist in new path, it should create the link:
    tmp_singularity.mount_file(old_path, new_path, filename)
    assert (new_path / filename).is_symlink()
    # This checks that the file linked to actually exists:
    assert (new_path / filename).exists()
    assert "File already linked or exists" not in caplog.text

    # if "filename" exists in new path, it should not fail:
    tmp_singularity.mount_file(old_path, new_path, filename)
    assert "File already linked or exists" in caplog.text


def test_mount_gear_home_to_tmp(tmpdir):
    """Unit test for mount_gear_home_to_tmp"""
    # create a few dirs in a fake FW_HOME:
    fw_home = str(tmpdir / "fw_home")
    fake_dirs = ["foo", "fam", "fim"]
    for d in fake_dirs:
        (Path(fw_home) / d).mkdir(parents=True)
    relative_fw_home = fw_home[1:] if (fw_home[0] == os.sep) else fw_home
    with patch("fw_gear_bids_hcp.tmp_singularity.FW_HOME", fw_home):
        # mount_gear_home_to_tmp cds to ".", and we want to create the fake dirs in
        # "fw_home", so change dirs:
        cwd = os.getcwd()
        os.chdir(fw_home)
        new_home = tmp_singularity.mount_gear_home_to_tmp(Path(fw_home))
        os.chdir(cwd)
    assert new_home == Path(fw_home, relative_fw_home)
    assert new_home.exists()
    for d in fake_dirs:
        assert (new_home / d).exists()


# Test 2 use cases:
# - FW_HOME is/is not writable:
@pytest.mark.parametrize("fw_home_writable", [True, False])
def test_start_singularity_fw_home_writable(tmpdir, fw_home_writable):
    """Unit test for start_singularity"""
    subfolder_name = "foo"
    user_given_writable_dir = tmpdir
    expected_mount_gear_home_to_tmp_return = "mount_gear_home_to_tmp_ran"
    # when fw_home is writable, we'll set it to tmpdir. If it is not writable, we'll
    # set it to a directory that doesn't exist ("/foo/bar"):
    fw_home = str(tmpdir) if fw_home_writable else "/foo/bar"
    expected_writable_dir = fw_home if fw_home_writable else user_given_writable_dir

    # We also check that if debug=True, we call log_singularity_details
    with patch("fw_gear_bids_hcp.tmp_singularity.FW_HOME", fw_home), patch(
        "fw_gear_bids_hcp.tmp_singularity.mount_gear_home_to_tmp",
        return_value=expected_mount_gear_home_to_tmp_return,
    ) as mock_mount_gear_home_to_tmp, patch(
        "fw_gear_bids_hcp.tmp_singularity.log_singularity_details"
    ) as mock_log_singularity_details:
        new_fw_home = tmp_singularity.start_singularity(
            subfolder_name, user_given_writable_dir=user_given_writable_dir, debug=True
        )
    mock_log_singularity_details.assert_called()
    mock_mount_gear_home_to_tmp.assert_called_once_with(Path(expected_writable_dir) / subfolder_name)
    assert new_fw_home == expected_mount_gear_home_to_tmp_return


def test_unlink_gear_mounts(tmpdir):
    """Unit test for unlink_gear_mounts"""
    # create a couple of folders in tmpdir/originals, and corresponding symlinks in
    # tmpdir/links:
    orig_folder = Path(tmpdir, "originals")
    links_folder = Path(tmpdir, "links")
    links_folder.mkdir()
    dummy_folders = ["foo", "bar"]
    for d in dummy_folders:
        (orig_folder / d).mkdir(parents=True)
        (links_folder / d).symlink_to(orig_folder / d)
    tmp_singularity.unlink_gear_mounts(links_folder)
    # make sure the links_folder is gone, but the originals are still there:
    assert not links_folder.exists()
    for d in dummy_folders:
        assert (orig_folder / d).exists()
