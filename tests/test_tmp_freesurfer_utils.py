import os
from pathlib import Path
from unittest.mock import patch

from fw_gear_bids_hcp import tmp_freesurfer_utils

# import pytest


def fake_unzip_archive(zipfile, destination):
    # create a folder two levels down from destination
    (Path(destination) / "fam" / "fim").mkdir(parents=True)


def test_check_subjects_dir_from_zip_file_path(tmpdir, caplog):
    """Unit test for check_subjects_dir_from_zip_file_path"""
    # create a folder with "previous results":
    previous_results = tmpdir / "previous_results"
    previous_results.mkdir()
    (previous_results / "foo.txt").ensure(file=True)  # 'touch' the file

    # don't create this folder, check_subjects_dir_from_zip_file_path will do it
    writable_dir = tmpdir / "writable_dir"

    with patch(
        "fw_gear_bids_hcp.tmp_freesurfer_utils.unzip_archive", wraps=fake_unzip_archive
    ) as mock_unzip_archive, patch("fw_gear_bids_hcp.tmp_freesurfer_utils.Path.rename") as mock_rename:
        tmp_freesurfer_utils.check_subjects_dir_from_zip_file_path(Path(writable_dir), Path(previous_results))
        assert "Using provided Freesurfer subject file" in caplog.text
        assert Path(writable_dir / "unzip-fs-subjects-dir").exists()
        mock_unzip_archive.assert_called_once()
        mock_rename.assert_called_once()


def test_set_FS_templates():
    """Unit test for set_FS_templates"""
    with patch("fw_gear_bids_hcp.tmp_freesurfer_utils.tmp_singularity.mount_file") as mock_mount_file:
        tmp_freesurfer_utils.set_FS_templates(Path("foo"))
        assert mock_mount_file.call_count == 3


def test_setup_freesurfer_for_singularity(tmpdir):
    """Unit test for setup_freesurfer_for_singularity"""
    tmp_freesurfer_utils.setup_freesurfer_for_singularity(tmpdir, tmpdir / "foo")
    assert os.environ["SUBJECTS_DIR"] == str(Path(tmpdir, "freesurfer", "subjects"))
